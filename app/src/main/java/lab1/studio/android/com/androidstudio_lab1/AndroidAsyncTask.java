package lab1.studio.android.com.androidstudio_lab1;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class AndroidAsyncTask extends AsyncTask<String, String, String> {
    String TAG = "HCMUT";
    private Context mContext;
    private VideoStreamingListener mListener;

    public AndroidAsyncTask(Context context, VideoStreamingListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    protected  String doInBackground(String... f_url) {
        String url = f_url[0];
        if (url.contains(".m3u8")) {
            Log.d(TAG, "url: " + url);
            return url;
        } else {
            try {
                //Make a GET request
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                HttpResponse response = httpclient.execute(httpget);
                // Get hold of the response entity
                HttpEntity entity = response.getEntity();

                Log.d(TAG, "doInBackground: " + entity);

                if (entity != null) {
                    // A Simple JSON Response Read
                    InputStream instream = entity.getContent();
                    String firstResponse = convertStreamToString(instream);

                    LinkModel objLink = new Gson().fromJson(firstResponse, LinkModel.class);

                    instream.close();
                    return objLink.getLow_res();
                }

            } catch (Exception e) {
                Log.d(TAG, "e: " + e);
            }
        }

        return "";
    }

    public interface VideoStreamingListener {
        void onStreamResponse(String link);
        void onStreamError(String message);
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected void onPostExecute(String path) {
        Log.d(TAG, "mListener: " + mListener);
        Log.d(TAG, "path: " + path);
        if (mListener == null) { return; }

        if(path.length() > 0) {
            mListener.onStreamResponse(path);
        } else {
            mListener.onStreamError(path);
        }

    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}