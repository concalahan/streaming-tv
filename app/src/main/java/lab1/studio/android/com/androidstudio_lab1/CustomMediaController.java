package lab1.studio.android.com.androidstudio_lab1;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.MediaController;
import android.widget.VideoView;

import static android.widget.FrameLayout.*;

public class CustomMediaController extends MediaController {

    public CustomMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
