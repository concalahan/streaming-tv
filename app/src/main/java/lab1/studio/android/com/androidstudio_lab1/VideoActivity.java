package lab1.studio.android.com.androidstudio_lab1;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class VideoActivity extends AppCompatActivity implements AndroidAsyncTask.VideoStreamingListener {
    private String TAG = "HCMUT";
    private VideoView videoView;
    private ProgressBar progressBar;
    private MediaController mediaController;
    private ImageButton fullScreen;

    private String link = "";
    private int pos;
    public Context context = this;
    public AndroidAsyncTask.VideoStreamingListener videoStreamingListener = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);

        hideNavigationBar();

        Intent intent = getIntent();
        String kenh = intent.getStringExtra("kenh");
        link = String.valueOf(Constants.CHANNEL_DICT.get(kenh.toUpperCase()));

        pos = new ArrayList<String>(Constants.CHANNEL_DICT.keySet()).indexOf(kenh.toUpperCase());

        TextView tv = this.findViewById(R.id.TextView03);
        tv.setSelected(true);

        videoView = findViewById(R.id.VideoView);
        progressBar = findViewById(R.id.progressBar);

        mediaController = new MediaController(VideoActivity.this);
        mediaController.setAnchorView(videoView);

        mediaController.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Handle prev click here
                List<String> l = new ArrayList<String>(Constants.CHANNEL_DICT.values());
                int temp = Constants.CHANNEL_DICT.size() - 1;

                if(pos == temp) {
                    link = String.valueOf(l.get(0));
                    pos = temp;
                } else {
                    link = String.valueOf(l.get(++pos));
                }
                new AndroidAsyncTask(context, videoStreamingListener).execute(link);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Handle next click here
                List<String> l = new ArrayList<String>(Constants.CHANNEL_DICT.values());
                int temp = Constants.CHANNEL_DICT.size() - 1;

                if(pos == 0) {
                    link = String.valueOf(l.get(temp));
                    pos = temp;
                } else {
                    link = String.valueOf(l.get(--pos));
                }
                new AndroidAsyncTask(context, videoStreamingListener).execute(link);
            }
        });

        new AndroidAsyncTask(this, this).execute(link);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onStreamResponse(String link) {
        videoView.setMediaController(mediaController);
        videoView.setVideoPath(link);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();

                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
                        progressBar.setVisibility(View.GONE);
                        hideNavigationBar();
                        mediaPlayer.start();
                    }
                });
            }
        });
    }

    @Override
    public void onStreamError(String message) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        videoView.start();
        //hideNavigationBar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

    private void hideNavigationBar() {
        this.getWindow().getDecorView()
                .setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                                View.SYSTEM_UI_FLAG_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                );
    }
}