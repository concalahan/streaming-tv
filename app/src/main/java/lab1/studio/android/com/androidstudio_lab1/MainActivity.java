package lab1.studio.android.com.androidstudio_lab1;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {
    String EXTRA_MESSAGE = "kenh";

    ArrayList<String> channel = new ArrayList<String>(){{
        add("vtv1");
        add("vtv2");
        add("vtv3");
        add("vtv4");
        add("vtv5");
        add("vtv6");
        add("vtv7");
        add("vtv8");
        add("vtv9");
    }};
    ListView listView;
    Button micButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a new Adapter
        final ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, channel);

        // display list view
        listView = findViewById(R.id.ListView);
        listView.setAdapter(listAdapter);

        // display mic button
        micButton = findViewById(R.id.button);
        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi-VN");
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Hi Speak Now ...");

                try {
                    startActivityForResult(intent,143);
                }
                catch (ActivityNotFoundException err) {
                    Log.d(Constants.TAG, "Activity Not Found :" + err);
                }
            }
        });

        // onclick event
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(view.getContext(), VideoActivity.class);

                intent.putExtra(EXTRA_MESSAGE, channel.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 143 && resultCode == RESULT_OK && data != null){
            ArrayList<String> voiceIntent = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String receiveText = voiceIntent.get(0).toLowerCase();



            for(int i=0; i<Constants.CHANNEL_DICT.keySet().size(); i++) {
                Log.d(Constants.TAG, "keySet: " + Constants.CHANNEL_DICT.keySet().toArray()[i].toString());

                if(receiveText.contains(Constants.CHANNEL_DICT.keySet().toArray()[i].toString().toLowerCase())) {
                    Log.d(Constants.TAG, "onActivityResult: hihihi");
                    Intent intent = new Intent(this, VideoActivity.class);
                    intent.putExtra(EXTRA_MESSAGE, channel.get(i));
                    startActivity(intent);
                }
                ;
            }
        }
    }

    public List<String> getChannel() {
        return this.channel;
    }
}
