package lab1.studio.android.com.androidstudio_lab1;

import java.util.LinkedHashMap;

public class Constants {
    public static final String TAG = "HCMUT";

    public static final LinkedHashMap<String, String> CHANNEL_DICT = new LinkedHashMap<String, String>() {
        {
            // VTV channels
            put("VTV1", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV1_HD/02.m3u8");
            put("VTV2", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV2_HD/02.m3u8");
            put("VTV3", "http://liverestreamobj.e66534d0.viettel-cdn.vn/hls/VTV3_HD/02.m3u8");
            put("VTV4", "http://binggodeals.com/channel/vtv4");
            put("VTV5", "http://binggodeals.com/channel/vtv5");
            put("VTV6", "http://binggodeals.com/channel/vtv6");
            put("VTV7", "http://binggodeals.com/channel/vtv7");
            put("VTV8", "http://binggodeals.com/channel/vtv8");
            put("VTV9", "http://binggodeals.com/channel/vtv9");
        }
    };
}
