package lab1.studio.android.com.androidstudio_lab1;

import com.google.gson.annotations.SerializedName;

public class LinkModel {
    @SerializedName("low_res")
    private String low_res;

    @SerializedName("mid_res")
    private String mid_res;

    @SerializedName("high_res")
    private String high_res;

    public String getLow_res() {
        return low_res;
    }

    public void setLow_res(String low_res) {
        this.low_res = low_res;
    }
}
